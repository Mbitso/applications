﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace MVCApplication.Models
{
    public class AuthorDBHandler
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["UFOR1"].ConnectionString);


        //Add New Author
        public bool AddAuthor(AuthorModel amodel)
        {
            SqlCommand cmd = new SqlCommand("Insert Into Z_Revos_MVC_Authors(Fname, Lname, City, Address) Values (@Fname, @Lname, @City, @Address)", con);
            cmd.Parameters.AddWithValue("@Fname", amodel.FName);
            cmd.Parameters.AddWithValue("@Lname", amodel.LName);
            cmd.Parameters.AddWithValue("@City", amodel.City);
            cmd.Parameters.AddWithValue("@Address", amodel.Address);

            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        //View Author Details

        public List<AuthorModel> GetAuthor()
        {
            List<AuthorModel> authorList = new List<AuthorModel>();
            //list<AuthorModel> authorList = new list<AuthorModel>();
            SqlCommand cmd = new SqlCommand("Select * from Z_Revos_MVC_Authors", con);
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach(DataRow dr in dt.Rows)
            {
                authorList.Add(
                    new AuthorModel
                    {
                        Id = Convert.ToInt32(dr["AuthorId"]),
                        FName = Convert.ToString(dr["Fname"]),
                        LName = Convert.ToString(dr["Lname"]),
                        City = Convert.ToString(dr["City"]),
                        Address = Convert.ToString(dr["Address"])
                    });
            }
            return authorList;
        }

        //Update Author Details
        public bool UpdateDetails(AuthorModel amodel)
        {
            SqlCommand cmd = new SqlCommand("Update Z_Revos_MVC_Authors Set Fname=@Fname, Lname=@Lname, City=@City, Address=@Address where AuthorId=@AuthorId", con);
            cmd.Parameters.AddWithValue("@AuthorId", amodel.Id);
            cmd.Parameters.AddWithValue("@Fname", amodel.FName);
            cmd.Parameters.AddWithValue("@Lname", amodel.LName);
            cmd.Parameters.AddWithValue("@City", amodel.City);
            cmd.Parameters.AddWithValue("@Address", amodel.Address);

            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        //Delete Author Details

        public bool DeleteAuthor(int id)
        {
            SqlCommand cmd = new SqlCommand("Delete from Z_Revos_MVC_Authors where AuthorId=@AuthorId", con);
            cmd.Parameters.AddWithValue("@AuthorId", id);

            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }
    }
}