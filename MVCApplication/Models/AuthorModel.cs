﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.ComponentModel.DataAnnotations;

namespace MVCApplication.Models
{

    public class AuthorModel
    {

        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["UFOR1"].ConnectionString);


        [Display(Name="AuthorId")]
        public int Id { get; set; }

        [Required(ErrorMessage ="First Name is required.")]
        public string FName { get; set; }

        [Required(ErrorMessage ="Last Name is required.")]
        public string LName { get; set; } 

        [Required(ErrorMessage ="City is required.")]
        public string City { get; set; }

        [Required(ErrorMessage ="Address is required.")]
        public string Address { get; set; }

    }
}