﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using MVCApplication.Models;

namespace MVCApplication.Controllers
{
    public class AuthorController : Controller
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["UFOR1"].ConnectionString);

        // GET: Author
        public ActionResult Index()
        {
            AuthorDBHandler dBHandler = new AuthorDBHandler();
            ModelState.Clear();
            return View(dBHandler.GetAuthor());
        }

        // GET: Author/Details/5
        public ActionResult Details(int id)
        {
            AuthorDBHandler adb = new AuthorDBHandler();
            ModelState.Clear();
            return View(adb.GetAuthor().Find(amodel => amodel.Id == id));
        }

        // GET: Author/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Author/Create
        [HttpPost]
        public ActionResult Create(AuthorModel amodel)
        {
            try
            {
                // TODO: Add insert logic here
                if(ModelState.IsValid)
                {
                    AuthorDBHandler adb = new AuthorDBHandler();
                    if(adb.AddAuthor(amodel))
                    {
                        ViewBag.Message = "Author Details Added Successfully";
                        ModelState.Clear();
                    }
                }
                return View();
            }
            catch
            {
                return View();
            }
        }

        // GET: Author/Edit/5
        public ActionResult Edit(int id)
        {
            AuthorDBHandler adb = new AuthorDBHandler();
            return View(adb.GetAuthor().Find(amodel=> amodel.Id==id));

        }

        // POST: Author/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, AuthorModel amodel)
        {
            try
            {
                // TODO: Add update logic here
                AuthorDBHandler adb = new AuthorDBHandler();
                adb.UpdateDetails(amodel);

                

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Author/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                // TODO: Add delete logic here
                AuthorDBHandler adb = new AuthorDBHandler();
                if(adb.DeleteAuthor(id))
                {
                    ViewBag.AlerMsg = "Author Deleted Successfully";
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // POST: Author/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
